/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 *
 * @author simon
 * Lebewesen und Gegenstände
 */
public class Objekt 
{
    JRPG app;
    int x, y;
    BufferedImage img;
    public Objekt(JRPG j, int xx, int yy)
    {
        app = j;
        x = xx;
        y = yy;
    }
    public void draw(Graphics g, int dx, int dy)
    {
        g.drawImage(img, dx, dy, 48, 48, app.ui.f);
    }
}
