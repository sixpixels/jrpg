/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author simon
 * eine Untergrundart
 */
public class Gras extends Untergrund
{
    public Gras(JRPG j)
    {
       super(j);
        try 
        {
            img = ImageIO.read(ClassLoader.getSystemClassLoader().getResource("Pictures/Landschaft/Gras.png"));
            imgDunkel = ImageIO.read(ClassLoader.getSystemClassLoader().getResource("Pictures/Landschaft/GrasDunkel.png"));
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
    
}
