/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author simon
 */
public class Mensch extends Rasse
{

    public Mensch(JRPG j) {
        super(j);
        try {
            img  = ImageIO.read(ClassLoader.getSystemClassLoader().getResource("Pictures/Persons/Mensch.png"));
        } catch (IOException ex) {
            Logger.getLogger(Mensch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
