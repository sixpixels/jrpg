/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.awt.Graphics;
import java.util.ArrayList;

/**
 *
 * @author simon
 * Spieler und NPC's
 */
public class Person extends Lebewesen
{
    ArrayList<Ausrüstung> ausrüstung = new ArrayList(); 
    Rasse rasse;
    public Person(JRPG j, int xx, int yy, Rasse r)
    {
        super(j, xx, yy);
        rasse = r;
        img = rasse.img;
    }
    @Override
    public void draw(Graphics g, int dx, int dy)
    {
        g.drawImage(img, dx, dy, 48, 48, app.ui.f);
        for(Ausrüstung a : ausrüstung)
        {
            if(a.sichtbar)
            {
                a.draw(g, dx, dy);
            }
        }
        
    }
    
}
