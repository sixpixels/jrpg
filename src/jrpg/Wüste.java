/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author simon
 * eine Untergrundart
 */
public class Wüste extends Untergrund
{
    public Wüste(JRPG j)
    {
       super(j);
        try 
        {
            img = ImageIO.read(ClassLoader.getSystemClassLoader().getResource("Pictures/Landschaft/Wüste.png"));
            imgDunkel = ImageIO.read(ClassLoader.getSystemClassLoader().getResource("Pictures/Landschaft/WüsteDunkel.png"));
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
    
}
