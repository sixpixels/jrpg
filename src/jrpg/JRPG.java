/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.util.ArrayList;

/**
 *
 * @author simon
 */
public class JRPG {

    /**
     * @param args the command line arguments
     * Die Hauptklasse von der aus alles gvesteuert wird
     */
    UI ui;
    Karte karte;
    Spieler spieler;
    ArrayList <Feld> felder;
    ArrayList <Lebewesen> lebewesen;
    Gras gras = new Gras(this);
    Wüste wüste = new Wüste(this);
    Wasser wasser = new Wasser(this);
    Weg weg = new Weg(this);
    Mensch m = new Mensch(this);
    public JRPG()
    {
        ui = new UI(this);
        felder = new ArrayList();
        lebewesen = new ArrayList();
        for(int xZähler = -1000; xZähler < 1000; xZähler++)                         //Hilfskonstruktion wird später aus file eingelesen
            for(int yZähler = -1000; yZähler < 1000; yZähler++)
            {
                if(Math.random() > 0.25)
                    felder.add(new Feld(this, xZähler, yZähler, gras));
                else if(Math.random() > 0.15)
                    felder.add(new Feld(this, xZähler, yZähler, weg));
                else if(Math.random() > 0.1)
                    felder.add(new Feld(this, xZähler, yZähler, wüste));
                else
                    felder.add(new Feld(this, xZähler, yZähler, wasser));
            }
        karte = new Karte(this);
        spieler = new Spieler(this, 30, 30, m);
        lebewesen.add(spieler);
        for(int i = 0; i < 100000; i++)
        {
            lebewesen.add(new NPC(this, (int)(Math.random() * 2000) - 1000,(int)(Math.random() * 2000) - 1000, m));
        }
        ui.start();
    }
    public static void main(String[] args) 
    {
        JRPG app = new JRPG();
    }
    
}
