/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author simon
 * das Graphische User Interface als eigener Thread
 * 
 */
public class UI extends Thread implements KeyListener{
    JRPG app;        //um ohne ständiges übertragen von Klassen auf alles zugreifen zu können       
    Frame f;      
    BufferedImage img;  //Für Graphikbeschleunigung
    int maxrange = 14;
        
    public UI(JRPG j)
    {
        app = j;
        init();
    }
    public void init()
    {                                       //erstellt neues Frame mit größe 800x600 und macht es möglich es zu schließen
        img = new java.awt.image.BufferedImage(800, 600, 1);
        f = new Frame("JRPG");
        f.setBounds(0, 400, 800, 600);
        f.setVisible(true);
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) { 
                System.exit(0); 
            }
        });
        f.addKeyListener(this);
    }
    @Override
    public void run()                                   //Zeichen Task zum zeichnen aller Objekte
    {
        while(true)
        {
            Graphics g = img.getGraphics();
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, 800, 600);
            app.karte.draw(g);
            f.getGraphics().drawImage(img, 0, 0, f);
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e)                  //Interaktionen mit dem Spieler über die tastatur
    {   
        
    }

    @Override
    public void keyReleased(KeyEvent e) 
    {
        int fx = app.spieler.x;
        int fy = app.spieler.y;                        
        if(e.getKeyCode() == KeyEvent.VK_W)
            app.spieler.y ++;
        if(e.getKeyCode() == KeyEvent.VK_A)
            app.spieler.x --;
        if(e.getKeyCode() == KeyEvent.VK_S)
            app.spieler.y --;
        if(e.getKeyCode() == KeyEvent.VK_D)
            app.spieler.x ++;
        if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
            System.exit(0);
        if(app.karte.getFeld(app.spieler.x, app.spieler.y).untergrund == app.wasser) //Um zu verhindern, dass man über wasser läuft
        {
            app.spieler.x = fx;
            app.spieler.y = fy;
        }
        for(Lebewesen l : app.lebewesen)
        {
            l.KI();
        }
        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {
            Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
