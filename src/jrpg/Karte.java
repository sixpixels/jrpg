/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

/**
 *
 * @author simon
 * Die Karte
 */
public class Karte 
{
    JRPG app;
    int sichtweite = 5;
    public Karte(JRPG j)
    {
        app = j;
    }
    public Feld getFeld(int x, int y)
    {
        for(Feld f : app.felder)
        {
            if(Math.sqrt(((f.x - app.spieler.x) * (f.x - app.spieler.x)) + ((f.y - app.spieler.y) * (f.y - app.spieler.y))) < 1)
                {
                    return f;
                }
        }
        System.out.print("getfeld");
        return app.felder.get(x * 1000 + y);
    }
    public void draw(Graphics g)                    //wird von UI aus aufgerufen
    {
        for(Feld f : app.felder)
        {
            if(Math.sqrt(((f.x - app.spieler.x) * (f.x - app.spieler.x)) + ((f.y - app.spieler.y) * (f.y - app.spieler.y))) < app.ui.maxrange)
            {
                if(Math.sqrt(((f.x - app.spieler.x) * (f.x - app.spieler.x)) + ((f.y - app.spieler.y) * (f.y - app.spieler.y))) < sichtweite)
                {
                    f.draw(g, true, 400 + (f.x - app.spieler.x) * 48, 300 - (f.y - app.spieler.y) * 48);
                    f.entdeckt = true;
                }
                else if(f.entdeckt)
                {
                    f.draw(g, false, 400 + (f.x - app.spieler.x) * 48, 300 - (f.y - app.spieler.y) * 48);

                }
                else
                {
                    g.fillRect(400 + (f.x - app.spieler.x) * 48, 300 - (f.y - app.spieler.y) * 48, 48, 48);
                }
            }
        }
        for(Lebewesen l : app.lebewesen)
        {
            if(Math.sqrt(((l.x - app.spieler.x) * (l.x - app.spieler.x)) + ((l.y - app.spieler.y) * (l.y - app.spieler.y))) < app.ui.maxrange)
            {
                if(Math.sqrt(((l.x - app.spieler.x) * (l.x - app.spieler.x)) + ((l.y - app.spieler.y) * (l.y - app.spieler.y))) < sichtweite)
                {
                    l.draw(g, 400 + (l.x - app.spieler.x) * 48, 300 - (l.y - app.spieler.y) * 48);
                }
            }
        }
    }
}
