/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 *
 * @author simon
 */
public class Untergrund 
{
    JRPG app;
    BufferedImage img, imgDunkel;
    public Untergrund(JRPG j)
    {
        app = j;
    }
    
    public void draw(Graphics g, boolean wirdGesehen, int x, int y)
    {
        if(wirdGesehen)
            g.drawImage(img, x, y, 48, 48, app.ui.f);
        else
            g.drawImage(imgDunkel, x, y, 48, 48, app.ui.f);
    }
    
}
