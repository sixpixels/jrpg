/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.awt.Graphics;

/**
 *
 * @author simon
 * Ein einzelnes Feld auf der Karte
 */
public class Feld 
{
    JRPG app;
    int x, y;
    boolean entdeckt = false;
    Untergrund untergrund;
    double myxFelder = 10;
    public Feld(JRPG j, int xx, int yy, Untergrund u)
    {
        app = j;
        x = xx;
        y = yy;
        untergrund = u;
    }
    public void draw(Graphics g, boolean wirdGesehen, int drawX, int drawY)
    {
        untergrund.draw(g, wirdGesehen, drawX, drawY);
    }
    
}
