/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jrpg;

import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author simon
 */
class Weg extends Untergrund{

    public Weg(JRPG j) {
        super(j);
        try 
        {
            img = ImageIO.read(ClassLoader.getSystemClassLoader().getResource("Pictures/Landschaft/Weg.png"));
            imgDunkel = ImageIO.read(ClassLoader.getSystemClassLoader().getResource("Pictures/Landschaft/WegDunkel.png"));
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
    
}
